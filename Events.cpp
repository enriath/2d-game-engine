#include "Events.h"

BaseEvent* Events::convertEvent(sf::Event e)
{
    switch(e.type)
    {
        case sf::Event::Closed:
        case sf::Event::LostFocus:
        case sf::Event::GainedFocus:
        case sf::Event::MouseEntered:
        case sf::Event::MouseLeft:
        
        return new BaseEvent(e.type);
        break;

        case sf::Event::KeyPressed:
        case sf::Event::KeyReleased:
        return new KeyEvent(e.type, e.key.code, e.key.alt, e.key.control, e.key.shift, e.key.system);
        break;

        case sf::Event::Resized:
        return new SizeEvent(e.type, e.size.width, e.size.height);
        break;

        default:
        return new BaseEvent(e.type);
        break;
    }
}
