#pragma once
#include <iostream>
#include <string>

#include "SFML/Graphics.hpp"
#include "UIElement.h"

class CUITextElement : public CUIElement
{
    sf::Font font;
    sf::Text text;
    
public:
    CUITextElement(){};

    CUITextElement(std::string fontPath);

    virtual void render(sf::RenderWindow &window);

    void loadFontFromFile(std::string path);

    std::string getText();

    void setText(std::string text);

    void getColour(int &R, int &G, int &B, int &A);

    void setColour(int R, int G, int B, int A);

    void getOutlineColour(int &R, int &G, int &B, int &A);

    void setOutlineColour(int R, int G, int B, int A);

    void setTextAnchor(int x, int y);

    float getOutlineThickness();

    void setOutlineThickness(float size);

    int getCharacterSize();

    void setCharacterSize(int size);
};