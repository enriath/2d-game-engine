#pragma once
#include <iostream>
#include <string>

#include "SFML/Graphics.hpp"
#include "UIElement.h"

class CUISpriteElement : public CUIElement
{
    sf::Sprite sprite;
    sf::Texture _texture;
    sf::Vector2u spriteSize;
    sf::Vector2f scalar;
    int colSize;
    int index;
    int tileWidth;
    int tileHeight;
    int numTextures;
    
public:
    CUISpriteElement();

    void loadTexture(std::string path, int x, int y, int w, int h);

    void loadTextureSheet(std::string path, int w, int h);

    void setTexture(int i, bool flip);

    virtual void render(sf::RenderWindow &window);

    void getSpriteSize(float &x, float &y)
    {
        x = spriteSize.x;
        y = spriteSize.y;
    }

    void setSpriteSize(float x, float y)
    {
        float scalarX = x / spriteSize.x;
        float scalarY = y / spriteSize.y;
        scalar = sf::Vector2f(scalarX, scalarY);
        sprite.setScale(scalarX, scalarY);
    }
};