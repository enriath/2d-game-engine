#include <chrono>
#include <list>
#include <vector>

#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "SFML/System/Vector2.hpp"

#include "Object.h"
#include "Backdrop.h"
#include "Events.h"
#include "UIElement.h"



class CGame
{

    //sf::Vector2u windowSize;
    sf::Vector2f cameraPos;

    std::list<CObject> objectList;

    sf::RenderWindow window;//(sf::VideoMode(200, 200), "SFML works!");
    sf::View _view;

    std::list<CObject>::iterator loopVar;

    sf::Event tempEvent;

    //CTerrain level;


protected:
    bool running = false;
    bool debug = false;
    

public:

    CGame()
    {
        window.create(sf::VideoMode(200, 200), "Generic Unnamed Engine Window");
        window.setKeyRepeatEnabled(false);
    }

    void setWindowTitle(char* title)
    {
        window.setTitle(title);
    }

    void setDebugState(bool state);

    void setWindowSize(int width, int height);

    //bool setTargetFramerate(double framerate);

    void addObject(CObject obj);

    void setCameraPosition(float x, float y)
    {
        cameraPos.x = x;
        cameraPos.y = y;
    }

    void renderObject(CObject *obj)
    {
        obj->render(window, cameraPos);
    }

    void renderUIElement(CUIElement *ele)
    {
        ele->render(window);
    }

    void renderLevel(CBackdrop *level, int windowWidth, int windowHeight)
    {
        level->render(window, windowWidth, windowHeight, cameraPos);
    }

    void gatherEvents(std::vector<BaseEvent*> &elist);

    void close()
    {
        window.close();
    }

    void windowClear()
    {
        window.clear();
    }

    void windowDisplay()
    {
        window.display();
    }

    bool getKeyState(int key);

    void updateAspectRatio(int width, int height);
};