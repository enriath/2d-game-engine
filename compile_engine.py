import platform
print(platform.python_version())

from distutils.core import setup
from Cython.Build import cythonize

setup(
    ext_modules = cythonize(
        "engine.pyx",
        language="c++")
)
