# 2D Game Engine

A 2D Game Engine built with Python 3, Cython, and C++ using the SFML framework. Created as the final year project at University

# How to build

Requirements: Windows 7+ 64 bit. Could potentially work on other systems, but would need some tweaking.

1. Install Python 3, preferably 3.6 as that was what the project was developed with. To help later, make sure you install the Py launcher.

2. Install `cython` with `pip`.

3. Install Visual Studio.

4. Download [SFML 2.4.2](https://www.sfml-dev.org/files/SFML-2.4.2-windows-vc14-64-bit.zip) and extract it into the same directory as `engine.pyx`.

5. Copy all the `.dll` files from `SFML-2.4.2/bin` into the root directory.

6. Run `compile_engine.bat`. If you didn't install the Py launcher in Step 1, or installed a different version of Python 3, you'll need to edit `compile_engine.bat` so that it will run on your Python installation.

# Examples

`pong.py`, `mario.py`, and `stresstest_objects.py` can be run for examples of working games. `stresstest_objects.py` requires `arial_7.ttf` to show text; Technically, this can be any TrueType Font file.

# Credits

The font used in the Mario example was created by DeviantArt user TheWolfBunny ([link](https://www.deviantart.com/thewolfbunny/art/Super-Mario-Bros-NES-Font-640775514))

The Mario sprites were obtained from The Spriters Resource ([link](https://www.spriters-resource.com/nes/supermariobros/)), and are owned by Nintendo

[SFML](https://www.sfml-dev.org/), [Python](https://www.python.org/), and [Cython](http://cython.org/) are owned by their respective owners.