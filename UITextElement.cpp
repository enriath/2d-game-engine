#include "UITextElement.h"

#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "SFML/System/Vector2.hpp"

CUITextElement::CUITextElement(std::string fontPath)
{
    loadFontFromFile(fontPath);
    text = sf::Text("testing", font);
}

void CUITextElement::loadFontFromFile(std::string path)
{
    font.loadFromFile(path);
}

std::string CUITextElement::getText()
{
    return text.getString();
}

void CUITextElement::setText(std::string newText)
{
    text.setString(newText);
}

void CUITextElement::getColour(int &R, int &G, int &B, int &A)
{
    sf::Color c = text.getFillColor();
    R = c.r;
    G = c.g;
    B = c.b;
    A = c.a;
}

void CUITextElement::setColour(int R, int G, int B, int A)
{
    text.setFillColor(sf::Color(R, G, B, A));
}

void CUITextElement::getOutlineColour(int &R, int &G, int &B, int &A)
{
    sf::Color c = text.getOutlineColor();
    R = c.r;
    G = c.g;
    B = c.b;
    A = c.a;
}

void CUITextElement::setOutlineColour(int R, int G, int B, int A)
{
    text.setOutlineColor(sf::Color(R, G, B, A));
}

float CUITextElement::getOutlineThickness()
{
    return text.getOutlineThickness();
}

void CUITextElement::setOutlineThickness(float size)
{
    text.setOutlineThickness(size);
}

int CUITextElement::getCharacterSize()
{
    return text.getCharacterSize();
}

void CUITextElement::setCharacterSize(int size)
{
    text.setCharacterSize(size);
}

void CUITextElement::render(sf::RenderWindow &window)
{
    text.setFont(font);
    sf::Vector2u wsize = window.getSize();
    text.setPosition(wsize.x * position.x - (text.getLocalBounds().width + text.getOutlineThickness()) * anchorPoint.x, 
        wsize.y * position.y - (text.getLocalBounds().height + text.getOutlineThickness()) * anchorPoint.y);
    window.draw(text);
}