#pragma once
#include <iostream>
#include <string>
#include <math.h>

#include "SFML/Graphics.hpp"

class CUIElement
{
protected:
    sf::Vector2f position;
    sf::Vector2f anchorPoint;
    
public:
    CUIElement();

    void init();

    void update(double deltaT);

    virtual void render(sf::RenderWindow &window);

    void getPosition(float &x, float &y);

    void setPosition(float x, float y);
    
    void addPosition(float x, float y);

    void setAnchorPosition(float x, float y);

    void getAnchorPosition(float &x, float &y);
};